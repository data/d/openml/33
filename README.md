# OpenML dataset: cylinder-bands

https://www.openml.org/d/33

**WARNING: This dataset is archived. Use [meta](https://gitlab.com/data/meta) to discuss it.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Bob Evans, RR Donnelley & Sons Co.  
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Cylinder+Bands) - August, 1995  
**Please cite**:   

**Cylinder bands**  
Process delays known as cylinder banding in rotogravure printing were substantially mitigated using control rules discovered by decision tree induction.
 
Attribute Information:
>
   1. timestamp: numeric;19500101 - 21001231 
   2. cylinder number: nominal
   3. customer: nominal; 
   4. job number: nominal; 
   5. grain screened: nominal; yes, no 
   6. ink color: nominal;  key, type 
   7. proof on ctd ink:  nominal;  yes, no  
   8. blade mfg: nominal;  benton, daetwyler, uddeholm 
   9. cylinder division: nominal;  gallatin, warsaw, mattoon 
  10. paper type: nominal;  uncoated, coated, super 
  11. ink type: nominal;  uncoated, coated, cover 
  12. direct steam: nominal; use; yes, no *
  13. solvent type: nominal;  xylol, lactol, naptha, line, other 
  14. type on cylinder:  nominal;  yes, no  
  15. press type: nominal; use; 70 wood hoe, 70 motter, 70 albert, 94 motter 
  16. press: nominal;  821, 802, 813, 824, 815, 816, 827, 828 
  17. unit number: nominal;  1, 2, 3, 4, 5, 6, 7, 8, 9, 10 
  18. cylinder size: nominal;  catalog, spiegel, tabloid 
  19. paper mill location: nominal; north us, south us, canadian, 
      scandanavian, mid european
  20. plating tank: nominal; 1910, 1911, other 
  21. proof cut: numeric;  0-100 
  22. viscosity: numeric;  0-100 
  23. caliper: numeric;  0-1.0 
  24. ink temperature: numeric;  5-30 
  25. humifity: numeric;  5-120 
  26. roughness: numeric;  0-2 
  27. blade pressure: numeric;  10-75 
  28. varnish pct: numeric;  0-100 
  29. press speed: numeric;  0-4000 
  30. ink pct: numeric;  0-100 
  31. solvent pct: numeric;  0-100 
  32. ESA Voltage: numeric;  0-16 
  33. ESA Amperage: numeric;  0-10 
  34. wax: numeric ;  0-4.0
  35. hardener:  numeric; 0-3.0 
  36. roller durometer:  numeric;  15-120 
  37. current density:  numeric;  20-50 
  38. anode space ratio:  numeric;  70-130 
  39. chrome content: numeric; 80-120 
  40. band type: nominal; class; band, no band

Notes:  
* cylinder number is an identifier and should be ignored when modelling the data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/33) of an [OpenML dataset](https://www.openml.org/d/33). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/33/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/33/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/33/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

